import numpy as np

def splitCount(total, denominations, knownCounts = []):
    #splitCount returns the number of ways a total sum can be acheived
    #using the denominations given by the user.  It also returns the memoization
    #list for future use if needed.
    countMemoization = knownCounts

    #The memoization list is initialized to impossible counts to denote
    #entries that have not been filled.
    if (len(countMemoization)==0):
        countMemoization = -1.0 *np.ones([len(denominations), total])

    outputCount = 0
    #If the total has already been memoized, return what has been memoized.
    if(len(denominations) > 0):
        if(countMemoization[len(denominations) - 1][total - 1] != -1.0):
            outputCount = countMemoization[len(denominations) - 1][total - 1]

    #Otherwise calculate split count via recursion.
        else:
            if (total < 0):
                outputCount = 0
            elif(total == 0):
                outputCount = 1
            else:
                outputCount = splitCount(total, denominations[1:], countMemoization)[0] + splitCount(total - denominations[0], denominations, countMemoization)[0]
                countMemoization[len(denominations) - 1][total - 1] = outputCount
    return outputCount, countMemoization

def initializeInputs(maxTotal, denominations):
    #Generates the memoization array and sorts the denominations
    knownCounts = -1.0 *np.ones([len(denominations), maxTotal])
    denominations = np.sort(denominations)
    return denominations, knownCounts

denominations = [1, 5, 10, 25]
denominations, knownCounts = initializeInputs(100, denominations)
currentSum = 0
for i in range(1, 101):
    if ((i%2) != 0):
        currentSum += splitCount(i, denominations, knownCounts)[0]
print "The sum of all ways of combining these denominations to form the odd totals between 1 and 100 is:"
print currentSum
if ((currentSum %2) == 0):
    print "Which is even."
else:
    print "Which is odd."
    
